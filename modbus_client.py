from pymodbus.client.sync import ModbusTcpClient
from pymodbus.client.sync import ModbusUdpClient
from pymodbus.client.sync import ModbusSerialClient
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.constants import Endian


class ModbusGatewayClient:

    def __init__(self):
        self.client = None
        self.connected = False
        self.mode = None # 'tcp', 'udp', 'rtu', 'ascii', 'enc'

    def connect(self, mode, kwargs):

        #TODO Implement encapsulated Modbus RTU over TCP (mode='enc')
        if mode =='tcp' or mode == 'udp':
            if mode == 'tcp':
                self.client = ModbusTcpClient(**kwargs)
                print('Trying connect tcp with parameters: {}'.format(kwargs))
            else:
                self.client = ModbusUdpClient(host=host, port=port)

            self.client.connect()


        elif mode == 'rtu' or mode == 'ascii':
            if mode == 'rtu':
                self.client = ModbusSerialClient(method='rtu', **kwargs)
                print('Trying connect rtu with parameters: {}'.format(kwargs))
            else:
                self.client = ModbusSerialClient(method='ascii', port=port, timeout=self.timeout, baudrate=baudrate)

            
            self.client.connect()
        else:
            return None

        if not self.client.is_socket_open():
            print("Cannot connect to modbus device using \"{}\" with arguments: {}".format(mode, kwargs))
        else:
            self.connected = True

  
    def disconnect(self):
        if self.client is not None:
            self.client.close()
            self.connected = False


    def read_outputs(self, unit, address, count):
        """ Modbus function code 01 - Read Coils
        #address – The starting address to read from
        #count – The number of coils to read
        #unit – The slave unit this request is targeting
        """
        return self.client.read_coils(unit, address, count)


    def write_outputs(self, unit, address, payload):
        """  Modbus function code 15 - Write Outputs
        :param address: The starting address to write to
        :param value: The value to write to the specified address
        :param unit: The slave unit this request is targeting
        :param count:
        :returns: True if write successful 
        """
        return self.client.write_coils(unit=unit, address=address, values=payload)


    def write_holding_registers(self, unit, address, payload):
        """ Modbus function code 16 - Write Outputs Registers
        :param address: The starting address to read from
        :param count: The number of discretes to read
        :param: unit: The slave unit this request is targeting
        :returns: True if write successful
        """
        return self.client.write_registers(unit=unit, address=address, values=payload)
 

    def read_holding_registers(self, unit, address, count):
        """ Modbus function code 03 - Read Outputs Registers
        :param address: The starting address to read from
        :param count: The number of discretes to read
        :param: unit: The slave unit this request is targeting
        #returns: The resulting read registers
        """
        return self.client.read_holding_registers(unit=unit, address=address, count=count)


    def read_inputs(self, unit, address, count):
        """ Modbus function code 02 - Read Inputs
        :param address: The starting address to read from
        :param count: The number of discretes to read
        :param: unit: The slave unit this request is targeting
        :returns: A deferred response handle
        """
        return self.client.read_discrete_inputs(unit=unit, address=address, count=count)


    def read_input_registers(self, unit, address, count):
        """ Modbus function code 04 - Read Inputs Registers
        :param address: The starting address to read from
        :param count: The number of discretes to read
        :param: unit: The slave unit this request is targeting
        :returns: A deferred response handle
         """
        return self.client.read_input_registers(unit=unit, address=address, count=count)
 
    def isConnected(self):
        return self.connected
    
    @staticmethod
    def decode(payload, data_type, count):
        """ Helper function for decoding Modbus registers values
        :data_type: Data type for decoding registers
        :count: Registers count
        :returns: Decoded registers values
        """
        
        byte_order = Endian.Big
        word_order = Endian.Little

        # TODO: Implement Modbus Endianness handling 
        #endianness = (byteorder = byte_order, word_order)

        values = [None] * count

        if data_type == 'bit':
            decoder = BinaryPayloadDecoder.fromRegisters(payload.bits,  byteorder=byte_order, wordorder=word_order)
            values = payload[0:count]
            return values
        elif data_type == 'uint16':
            decoder = BinaryPayloadDecoder.fromRegisters(payload.registers,  byteorder=byte_order, wordorder=word_order)
            for x in range(count):
                values[x] = decoder.decode_16bit_uint()
            return values
        elif data_type == 'int16':
            decoder = BinaryPayloadDecoder.fromRegisters(payload.registers,  byteorder=byte_order, wordorder=word_order)
            for x in range(count):
                values[x] = decoder.decode_16bit_int()
            return values
        elif data_type == 'uint32':
            decoder = BinaryPayloadDecoder.fromRegisters(payload.registers,  byteorder=byte_order, wordorder=word_order)
            for x in range(count):
                values[x] = decoder.decode_32bit_uint()
            return values
        elif data_type == 'int32':
            decoder = BinaryPayloadDecoder.fromRegisters(payload.registers,  byteorder=byte_order, wordorder=word_order)
            for x in range(count):
                values[x] = decoder.decode_32bit_int()
            return values
        elif data_type == 'float32':
            decoder = BinaryPayloadDecoder.fromRegisters(payload.registers,  byteorder=byte_order, wordorder=word_order)
            for x in range(count):
                values[x] = decoder.decode_32bit_float()
            return values
        elif data_type == 'int16:hex':
            decoder = BinaryPayloadDecoder.fromRegisters(payload.registers,  byteorder=byte_order, wordorder=word_order)
            for x in range(count):
                values[x] = hex(decoder.decode_16bit_int())
            return values
        else:
            return


    @staticmethod
    def build_payload(value, data_type, count):

        # TODO: Implement Modbus Endianness handling 
        builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Little)

        if data_type == 'bit':
            payload= [value] * count
            return payload

        elif data_type == 'uint16':
            for x in range(count):
                builder.add_16bit_uint(value)
            payload = builder.to_registers()
            return payload

        elif data_type == 'int16':
            for x in range(count):
                builder.add_16bit_int(value)
            payload = builder.to_registers()
            return payload

        elif data_type == 'uint32':
            for x in range(count):
                builder.add_32bit_uint(value)
            payload = builder.to_registers()
            return payload

        elif data_type == 'int32':
            for x in range(count):
                builder.add_32bit_int(value)
            payload = builder.to_registers()
            return payload

        elif data_type == 'float32':
            for x in range(count):
                builder.add_32bit_float(value)
            payload = builder.to_registers()
            return payload
        else:
            return