import os
import glob
import time
import json
from pathlib import Path

RESPONSE_PATH = '/tmp/communication/modbus/response/'
REQUEST_PATH  = '/tmp/communication/modbus/request/'

DEVICE_1_ID =       'dev1'
DEVICE_2_ID =       'dev2'


def write_request(request):

    try:
        json_object = json.loads(request)
        with open(os.path.join(REQUEST_PATH, request), 'w') as f:
            return json_object
    except ValueError as e:
        print("Not valid json string: {}".format(request))

    return




def read_response():

    response = {}

    while True:
        rs_files = glob.glob(os.path.join(RESPONSE_PATH,'*'))    # '*' accept any file format
        if rs_files:
            rs_file = min(rs_files, key=os.path.getctime)
            response = os.path.basename(rs_file)
            
            try:
                os.remove(rs_file)
            except OSError:
                pass

            break
        time.sleep(0.1)

    return response


def main():

    #Create directory for response writing
    if not os.path.exists(REQUEST_PATH):
        os.makedirs(REQUEST_PATH)

    while True:

        #Test request: Write values to modbus "tcp" device holding registers
        # request = '{"id": "%s", "ts": %0.7f, "m": "tcp", "h": "192.168.1.100", "p": 5020, "u": 1, "a": 0, "c": 5, "v": 79.78, "dt": "4:float"}' % (DEVICE_1_ID, time.time())
        # print("Sending device: {} request: {}".format(DEVICE_1_ID, request))
        # if write_request(request):
        #     print("Waiting {} response ...".format(DEVICE_1_ID))
        #     response = read_response()
        #     print("Received device: {} response: {}".format(DEVICE_1_ID, response))

        
        #Test request: Read input status from modbus "tcp" device
        # request = '{"id": "%s", "ts": %0.7f, "m": "tcp", "h": "192.168.1.100", "p": 5020, "u": 1, "a": 0, "c": 5, "v": null, "dt": "1"}' % (DEVICE_1_ID, time.time())
        # print("Sending device: {} request: {}".format(DEVICE_1_ID, request))
        # if write_request(request):
        #     print("Waiting {} response ...".format(DEVICE_1_ID))
        #     response = read_response()
        #     print("Received device: {} response: {}".format(DEVICE_1_ID, response))


        #Test request: Read values from modbus "rtu" device input registers
        request = '{"id": "%s", "ts": %0.7f, "m": "rtu", "h": "ttyUSB0", "b": 9600, "d": 8, "s": 2, "p": "N", "u": 90, "a": 1004, "c": 4, "v": null, "dt": "3"}' % (DEVICE_2_ID, time.time())
        print("Sending device: {} request: {}".format(DEVICE_1_ID, request))
        if write_request(request):
            print("Waiting {} response ...".format(DEVICE_1_ID))
            response = read_response()
            print("Received device: {} response: {}".format(DEVICE_1_ID, response))

        time.sleep(1)

if __name__ == "__main__":
     main()