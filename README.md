### modbus-agent


## **Short modbus protocol communication test agent usage**


**Communication:**

Client writes modbus request as file name (json string) to RAM directory:

`'/tmp/communication/modbus/request/'`

Client reads modbus response as file name (json string) from RAM directory:

`'/tmp/communication/modbus/response/'`

**Serial RTU communication:**

Request mesage:

`{"id": "dev2", "ts": 1578495559.5148492, "m": "rtu", "h": "ttyUSB0", "b": 9600, "d": 8, "s": 2, "p": "N", "u": 90, "a": 1004, "c": 4, "v": null, "dt": "3"}`

Read modbus device  4 input registers starting at address 1004 using serial RTU communication

Received mesage example:

`{"id": "dev2", "ts": 1578495559.5148492, "m": "rtu", "h": "ttyUSB0", "b": 9600, "d": 8, "s": 2, "p": "N", "u": 90, "a": 1004, "c": 4, "v": [115, 250, 58, 7], "dt": "3"}`

Registers values (1004... 1007) = `[115, 250, 58, 7]`

**TCP/IP communication:**

Request mesage:

`{"id": "dev1", "ts": 1578495559.5148492, "m": "tcp", "h": "192.168.1.100", "p": 5020, "u": 1, "a": 0, "c": 3, "v": 79.78, "dt": "4:float"}'`

Write modbus device  3 holding registers starting at address 0 using TCP/IP communication

Received mesage example:
`{"id": "dev1", "ts": 1578495559.5148492, "m": "tcp", "h": "192.168.1.100", "p": 5020, "u": 1, "a": 0, "c": 5, "v": [79.78, 79.78, 79.78], "dt": "4:float"}'`

Registers values (0... 2) = `[79.78, 79.78, 79.78]`

**Arguments:**

Modbus serial (RTU) communication arguments: `('h', 'b', 'd', 's', 'p')`

```
h  - Serial port when using ModBus RTU protocol
b  - Baudrate (e.g. 9600, 19200, ...)
d  - Databits (7 or 8)
s  - Stopbits (1 or 2)
p  - Parity (N - none, E - even, O - odd)
```

Modbus TCP/IP communication arguments: `('h', 'p')`

```
h  - Host name or dotted IP address when using MODBUS/TCP protocol
p  - IP protocol port number

```

Modbus protocol arguments: `('u', 'a', 'c', 'v', 'dt')`

```
u  - Slave address (1-255)
a  - Register start reference address
c  - Number of values to read
v  - If Value != None write value to registers. If value = None read from register
dt - Modbus protocol data type
```

Modbus data types: `(dt)`

```
dt 0          Discrete output data type (binary 0 or 1)
dt 1          Discrete input data type (binary 0 or 1)
dt 3          16-bit input register data type
dt 3:int      32-bit integer data type in input register table
dt 3:float    32-bit float data type in input register table
dt 4          16-bit output (holding) register data type (default)
dt 4:int      32-bit integer data type in output (holding) register table
dt 4:float    32-bit float data type in output (holding) register table
```