import os
import glob
import time
import json
from pathlib import Path
from modbus_client import ModbusGatewayClient

RESPONSE_PATH = '/tmp/communication/modbus/response/'
REQUEST_PATH  = '/tmp/communication/modbus/request/'


ETHERNET_SHEMA = {
     'host': '192.168.1.100',   #h  - Host name or dotted IP address when using MODBUS/TCP protocol
     'port': 520                #p  - IP protocol port number
 }

SERIAL_SHEMA = {
    'port': 'COM',              #h  - Serial port when using ModBus RTU protocol
    'baudrate': 9600,           #b  - Baudrate (e.g. 9600, 19200, ...)
    'databits': 8,              #d  - Databits (7 or 8)
    'stopbits': 2,              #s  - Stopbits (1 or 2)
    'parity': 'N'               #p  - Parity (N - none, E - even, O - odd)
}

MODBUS_SCHEMA = {
    'unit': 1,                  #u  - Slave address (1-255)
    'address': 0,               #a  - Register start reference address
    'count': 1,                 #c  - Number of values to read
    'value': None,              #v  - If Value != None write value to registers. If value = None read from register
    'data_type': 1              #dt - Modbus protocol data type
}

REQUEST_SHEMA = {
    'device': ('id', 'ts'),
    'modes': ('tcp', 'rtu'),
    'eth_arguments': ('h', 'p'),
    'ser_arguments': ('h', 'b', 'd', 's', 'p'),
    'mb_arguments': ('u', 'a', 'c', 'v', 'dt')
}

FAIL_MESAGE = 'FAILED'

class ModbusAgent:

    def __init__(self):
        # Modbus client parameters
        self.client = None
        self.client_connected = False
        self.client_mode = None
        self.client_conn_param = ETHERNET_SHEMA

        # Modbus request parameters
        self.device_id = ''
        self.device_ts = None
        self.request_path = ''
        self.request = {}
        self.rq_mode =  None
        self.rq_conn_param = {}
        self.rq_mb_param = MODBUS_SCHEMA

        # Modbus response parameters
        self.mb_response = []
        self.failed = False

   
    def get_request(self):

        rq_files = glob.glob(os.path.join(REQUEST_PATH,'*'))    # '*' accept any file format

        if rq_files:

            rq_file = min(rq_files, key=os.path.getctime)
            self.request_path = rq_file

            try:
                self.request = json.loads(os.path.basename(rq_file))
                print('REQUEST: ' + str(self.request))
            except:
                print("Cannot load request: {} as JSON object".format(os.path.basename(rq_file)))
                self.failed = True
                self.write_response()
                self.delete_request()          


    def delete_request(self):
        
        try:
            os.remove(self.request_path)
        except:
            print("Error while deleting file {}".format(self.request_path))
        
        self.request.clear()
        self.request_path = ''
 

    def check_request(self):

        dev_id = None
        dev_ts = None
        rq_mode = None
        rq_conn_param = {}
        rq_mb_param = MODBUS_SCHEMA

        #Check device indetification        
        if all(arg in self.request for arg in REQUEST_SHEMA['device']):
            dev_id = self.request['id']
            dev_ts = self.request['ts']
        else:
            print ("ERROR: wrong or missing device indentification")
            return


        #Check Modbus connection method
        if 'm' in self.request and any(self.request['m'] == m for m in REQUEST_SHEMA['modes']):
            rq_mode = self.request['m']
        else:
            print ("ERROR: wrong or missing connection parameter 'm'")
            return

        #Check Modbus connection method parameters
        if rq_mode == 'tcp' and len(self.request)==10 and all(arg in self.request for arg in REQUEST_SHEMA['eth_arguments']):
            rq_conn_param = ETHERNET_SHEMA
            rq_conn_param['host'] = self.request['h']
            rq_conn_param['port'] = self.request['p']
        elif rq_mode == 'rtu' and len(self.request)==13 and all(arg in self.request for arg in REQUEST_SHEMA['ser_arguments']):
            rq_conn_param = SERIAL_SHEMA
            rq_conn_param['port'] = '/dev/' + str(self.request['h'])
            rq_conn_param['baudrate'] = self.request['b']
            rq_conn_param['databits'] = self.request['d']
            rq_conn_param['stopbits'] = self.request['s']
            rq_conn_param['parity'] = self.request['p']
        else:
            print ("ERROR: missing or wrong {} connection parameters".format(rq_mode))
            return

        #Check Modbus parameters
        if all(arg in self.request for arg in REQUEST_SHEMA['mb_arguments']):
            rq_mb_param['unit'] = self.request['u']
            rq_mb_param['address'] = self.request['a']
            rq_mb_param['count']  = self.request['c']
            rq_mb_param['value']  = self.request['v']
            rq_mb_param['data_type'] = self.request['dt']
        else:
            print ("ERROR: missing or wrong modbus  parameters")
            return
        
        #Check passed. Assign parameters
        self.device_id = dev_id
        self.device_ts = dev_ts
        self.rq_mode = rq_mode
        self.rq_conn_param = rq_conn_param
        self.rq_mb_param = rq_mb_param

    
    def execute_request(self):

        if self.client is None:
            self.client = ModbusGatewayClient()

        if not self.client.isConnected():
            try:
                self.client.connect(self.rq_mode, self.rq_conn_param)
            except Exception:
                print("Failed to connect in {} mode with parameters : {}".format(self.rq_mode, self.rq_conn_param))
        #Check if modbus request can use existing connection
        elif self.client_mode != self.rq_mode or self.client_conn_param != self.rq_conn_param:
            self.client.disconnect()
            try:
                self.client.connect(self.rq_mode, self.rq_conn_param)
            except Exception:
                print("Failed to connect in {} mode with parameters : {}".format(self.rq_mode, self.rq_conn_param))
        else:
            pass

        if self.client.isConnected():

            try:
                if self.rq_mb_param['value'] is None:
                    self.execute_read_request(**self.rq_mb_param)
                else:
                    self.execute_write_request(**self.rq_mb_param)
            except:
                print ("Modbus {} communication error ".format(self.rq_mode))
                self.failed = True

            self.client_mode = self.rq_mode
            self.client_conn_param = self.rq_conn_param
        else:
            self.failed = True


    def execute_read_request(self, unit, address, count, value, data_type):
        """
        dt 0          Discrete output data type (binary 0 or 1)
        dt 1          Discrete input data type (binary 0 or 1)
        dt 3          16-bit input register data type
        dt 3:int      32-bit integer data type in input register table
        dt 3:float    32-bit float data type in input register table
        dt 4          16-bit output (holding) register data type (default)
        dt 4:int      32-bit integer data type in output (holding) register table
        dt 4:float    32-bit float data type in output (holding) register table
        """

        if data_type == '0':
            result = self.client.read_outputs(unit, address, count)
            if not result.isError():
                self.mb_response = self.client.decode(result, 'bit', count)
            else:
                self.failed = True

        elif data_type == '1':
            result = self.client.read_inputs(unit, address, count)
            if not result.isError():
                self.mb_response = self.client.decode(result, 'bit', count)
            else:
                self.failed = True  

        elif data_type == '3':
            result = self.client.read_input_registers(unit, address, count)
            if not result.isError():
                self.mb_response = self.client.decode(result, 'uint16', count)
            else:
                self.failed = True

        elif data_type == '3:int16':
            result = self.client.read_input_registers(unit, address, (count * 2))
            if not result.isError():
                self.mb_response = self.client.decode(result, 'int16', count)
            else:
                self.failed = True

        elif data_type == '3:int':
            result = self.client.read_input_registers(unit, address, (count * 2))
            if not result.isError():
                self.mb_response = self.client.decode(result, 'int32', count)
            else:
                self.failed = True

        elif data_type == '3:float':
            result = self.client.read_input_registers(unit, address, (count * 2))
            if not result.isError():
                self.mb_response = self.client.decode(result, 'float32', count)
            else:
                self.failed = True

        elif data_type == '4':
            result = self.client.read_holding_registers(unit, address, count)
            if not result.isError():
                self.mb_response = self.client.decode(result, 'uint16', count)
            else:
                self.failed = True

        elif data_type == '4:int16':
            result = self.client.read_holding_registers(unit, address, count)
            if not result.isError():
                self.mb_response = self.client.decode(result, 'int16', count)
            else:
                self.failed = True

        elif data_type == '4:int':
            result = self.client.read_holding_registers(unit, address, count)
            if not result.isError():
                self.mb_response = self.client.decode(result, 'int32', count)
            else:
                self.failed = True

        elif data_type == '4:float':
            result = self.client.read_holding_registers(unit, address, (count * 2))
            if not result.isError():
                self.mb_response = self.client.decode(result, 'float32', count)
            else:
                self.failed = True
        else:
            print('Unknown modbus data type \'{}\''.format(data_type))


    def execute_write_request(self, unit, address, count, value, data_type):
        """
        dt 0          Discrete output (coil) data type (binary 0 or 1)
        dt 4          16-bit output (holding) register data type (default)
        dt 4:int16    16-bit output (holding) register data type with signed int display
        dt 4:int      32-bit integer data type in output (holding) register table
        dt 4:float    32-bit float data type in output (holding) register table
        """
        if data_type == '0':
            payload = self.client.build_payload(value, 'bit', count)
            result = self.client.write_coils(unit, address, payload)
            if not result.isError():
                self.mb_response = [value]*count
            else:
                self.failed = True

        if data_type == '4':
            payload = self.client.build_payload(value, 'uint16', count)
            result = self.client.write_holding_registers(unit, address, payload)
            if not result.isError():
                self.mb_response = [value]*count
            else:
                self.failed = True

        if data_type == '4:int':
            payload = self.client.build_payload(value, 'int32', count)
            result = self.client.write_holding_registers(unit, address, payload)
            if not result.isError():
                self.mb_response = [value]*count
            else:
                self.failed = True

        if data_type == '4:float':
            payload = self.client.build_payload(value, 'float32', count)
            result = self.client.write_holding_registers(unit, address, payload)
            if not result.isError():
                self.mb_response = [value]*count
            else:
                self.failed = True


    def write_response(self):

        if not self.failed:
            response = self.request
            response['ts'] = time.time()
            response['v'] = self.mb_response
        else:
            response = {'id': self.device_id, 'ts': time.time(), 'error': FAIL_MESAGE}

        try:
            rs = json.dumps(response)
            with open(os.path.join(RESPONSE_PATH, rs), 'w') as f:
                pass
            print("RESPONSE: {}".format(rs))
        except:
            print("Failed to write response: {}".format(response))

        self.failed = False


    def run(self):

        while True:
            self.get_request()
            
            if self.request:
                self.check_request()
                self.execute_request()
                self.write_response()
                self.delete_request()
            else:
                pass

            time.sleep(0.01)


def main():

    #Create directory for response writing
    if not os.path.exists(RESPONSE_PATH):
        os.makedirs(RESPONSE_PATH)

    agent = ModbusAgent()
    agent.run()


if __name__ == "__main__":
     main()