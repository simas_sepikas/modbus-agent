import os
import asyncio
import time
import struct


EXCHANGE_DIR = '/tmp/comunications/'
REQUEST_PIPE = 'to.modbus.serial'
RESPONSE_PIPE = 'from.modbus.serial'

loop = asyncio.get_event_loop()

#Create pipes
if not os.path.exists(EXCHANGE_DIR):
    os.makedirs(EXCHANGE_DIR)

try:
    os.mkfifo(EXCHANGE_DIR + REQUEST_PIPE)

except FileExistsError:
    print("named pipe file {} already exists".format(REQUEST_PIPE))

try:
    os.mkfifo(EXCHANGE_DIR + RESPONSE_PIPE)
except FileExistsError:
    print("named pipe file {} already exists".format(RESPONSE_PIPE))


def decode_msg_size(size_bytes: bytes) -> int:
    return struct.unpack("<I", size_bytes)[0]


def decode_msg(content: bytes) -> str:
    return content.decode('utf8')


def pipeArm(file, callback):
    """
    Open named pipe and add pipe fd into event loop for reading. When pipe
    will be available for read, event loop will trigger provided callback.
    When we set callback for pipe, we will call this as "armed pipe".
    """
    fd = os.open(file, os.O_RDONLY | os.O_NONBLOCK)
    loop.add_reader(fd, pipeConsume, fd, file, callback)


def pipeConsume(fd, file, callback):
    """
    Read data from named pipe. This function will be called as callback from
    event loop. Consume data from named event loop and re-arm pipe for later
    data.
    """
    msg_size_bytes = os.read(fd, 4)
    msg_size = decode_msg_size(msg_size_bytes)
    data = os.read(fd, msg_size).decode("utf8")
    callback(data)

    """
    After reading from pipe we need to close this file and reopen for read
    again. Otherwise even loop fd reader will understand that here is same
    data inside pipe so will start to read again and again.

    Reopen file and reassign fd before doing pipe re-arm to prevent 
    infinitive loop.
    """
    os.close(fd)
    loop.remove_reader(fd)
    
    pipeArm(file, callback)


def testCallback(data):
    print("callback - {}".format(data))


def run():

    pipeArm(EXCHANGE_DIR + REQUEST_PIPE, testCallback)

    try:
        loop.run_forever()
    finally:
        loop.close()


if __name__ == "__main__":
     run()