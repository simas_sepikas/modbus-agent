import os
import sys
import time
import json
import struct

EXCHANGE_DIR = '/tmp/comunications/'
REQUEST_PIPE = 'to.modbus.serial'
RESPONSE_PIPE = 'from.modbus.serial'

REQUEST_EXAMPLE = {
    'tid': 1,
    'mode': 'rtu',
    'port': 'ttyUSB0',
    'baudrate': 9600,
    'databits': 8,
    'stopbits': 2,
    'parity': 'N',
    'timeout': 1,
    'unit': 90,
    'address': 0,
    'cmd': 3,
    'count': 4,
    'value': None,
    'dt': 'uint16'
}

def encode_msg_size(size: int) -> bytes:
    return struct.pack("<I", size)


def create_msg(content: bytes) -> bytes:
    size = len(content)
    return encode_msg_size(size) + content.encode('utf8')


def main():

    output_fd = None
    tid = 0

    try:
        output_fd = os.open(EXCHANGE_DIR + REQUEST_PIPE, os.O_WRONLY)
        listening = True
    except FileNotFoundError:
        print("Named pipe file {} doesn't exists. Probaly pipe listener not running".format(EXCHANGE_DIR + REQUEST_PIPE))
        sys.exit(0)
        
    try:
        while True:
            payload = REQUEST_EXAMPLE
            payload['tid'] = tid
            msg = create_msg(json.dumps(payload))
            try:
                os.write(output_fd, msg)
            except BrokenPipeError:
                print("Pipe listener disconnected")
            tid += 1
            time.sleep(1)
    except KeyboardInterrupt:
        print("Closing pipe writer")
    finally:
        os.close(output_fd)


if __name__ == "__main__":
     main()