import zmq
import random
import time


EXCHANGE_DIR = 'ipc:///tmp/comunications/'
REQUEST_SOCKET = 'to.modbus.serial'
RESPONSE_SOCKET= 'from.modbus.serial'

REQUEST_EXAMPLE = {
    'tid': 1,
    'mode': 'rtu',
    'port': 'ttyUSB0',
    'baudrate': 9600,
    'databits': 8,
    'stopbits': 2,
    'parity': 'N',
    'timeout': 1,
    'unit': 90,
    'address': 0,
    'cmd': 3,
    'count': 4,
    'value': None,
    'dt': 'uint16'
}

context = zmq.Context()
pub_socket = context.socket(zmq.PUB)
sub_socket = context.socket(zmq.SUB)
sub_socket.setsockopt(zmq.SUBSCRIBE, b'')

pub_socket.bind(EXCHANGE_DIR + REQUEST_SOCKET)
sub_socket.connect(EXCHANGE_DIR + RESPONSE_SOCKET)

#Wait for connections
time.sleep(1)

tid = 0
request = REQUEST_EXAMPLE


def write_request_batch(tid, request):
    for i in range(5):
        request['tid'] = tid
        request['value'] = random.expovariate(1/5)
        payload = str(request).encode('utf8')
        print("Sending: {}".format(payload))
        pub_socket.send(payload)
        tid += 1

    
while True:
    write_request_batch(tid, request)
    time.sleep(5)

    while True:
        try:
            msg = sub_socket.recv(zmq.DONTWAIT)
            print(msg.decode('utf8'))
        except zmq.Again:
            break
        # process task

context.term()
