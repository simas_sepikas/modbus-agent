import zmq
import time

EXCHANGE_DIR = 'ipc:///tmp/comunications/'
REQUEST_SOCKET = 'to.modbus.serial'
RESPONSE_SOCKET= 'from.modbus.serial'



context = zmq.Context()
pub_socket = context.socket(zmq.PUB)
sub_socket = context.socket(zmq.SUB)
sub_socket.setsockopt(zmq.SUBSCRIBE, b'')

sub_socket.connect(EXCHANGE_DIR + REQUEST_SOCKET)
pub_socket.bind(EXCHANGE_DIR + RESPONSE_SOCKET)


while True:
    msg = sub_socket.recv()
    if msg:
        print("Received form client: {}".format(msg))
        response = "Echo from server: {}".format(msg)
        pub_socket.send(response.encode('utf8'))
        time.sleep(0.5)

context.term()