# File: example-mqtt.py
#
# This is demo script on how to use mqtt connection in aispeka firmware
# by using paho-mqtt lib as mqtt client.
#

import os
import time
import json
import paho.mqtt.client as mqtt

THINGSBOARD_HOST = '684008.s.dedikuoti.lt'
#DEVICE_ACCESS_TOKEN = 'PlquR8ULLSAKfjpYLXee' #RPI
DEVICE_ACCESS_TOKEN = 'FYXUVHDZQp4F57IFC6nD' #iot2000


# on_connect will be called when connection process is finished.
# it will notify about successful connection or failed connection.
def on_connect(client, userdata, rc, *extra_params):
    print("connection result rc={}".format(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe('v1/devices/me/attributes', 1)
    client.subscribe('v1/devices/me/attributes/response/+', 1)
    client.subscribe('v1/devices/me/rpc/request/+', 1)

    return

# on_message will be called when client receives message
# on subscribed topics.
def on_message(client, userdata, msg):
    print("Received mesage; topic = '{}', message = {}".format(msg.topic, msg.payload))
    
    if msg.topic.startswith( 'v1/devices/me/rpc/request/'):

        requestId = msg.topic[len('v1/devices/me/rpc/request/'):len(msg.topic)]
        print("This is a RPC call. RequestID: {}".format(requestId))
        response = {'rpc_response': 'received'}
        payload = "v1/devices/me/rpc/response/" + rquestId + json.dump(response, 1)
        client.publish(payload, 1)

    return


client = mqtt.Client()
client.username_pw_set(DEVICE_ACCESS_TOKEN)
client.on_connect = on_connect
client.on_message = on_message
client.connect(THINGSBOARD_HOST, 1883, 60)
client.loop_start()


while True:
    time.sleep(1)
    topic = 'v1/devices/me/telemetry'

    data = {
        'ts': int(time.time()),
        'firmware': os.uname().release
    }
    payload = json.dumps(data, 1)
    print("publishing to topic='{}', payload='{}'".format(topic, payload))
    
    client.publish(topic, payload)